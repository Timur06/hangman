package at.ert.cc.hangman;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Hangman {


    public static final String[] WORDS = {
        "KLAVIER", "MOZART", "GITTARE", "SCHWIMMBAD", "HAUS","ARGENTINIEN", "KREDITKARTE", "ZAUBERSPRUCH", "VAMPIR",
        "MUSIK", "MAFIA", "ITALIEN", "JAVA", "ZAUBERER", "SUPERMODEL", "DRACHEN", "FILM", "APFEL", "BIRNE", "VORARLBERG",
        "MAGIE", "MAGISCH", "KLEIDERSCHRANK", "SANDWICH", "BILDERBUCH", "MEXIKO", "AUSTRALIEN", "COMIC", "NUDELAUFLAUF",
        "ENGLISCH", "BEZIEHUNG", "TIERE", "BROOKLYN", "THEMA", "SOFTWARE", "PROGRAMMIERUNG", "PROGRAMMIEREN", "HANGMAN"
    };

    public static final Random RANDOM = new Random();
    public static final int MAX_ERRORS = 8;
    private String wordToFind;
    private char[] wordFound;
    private int nbErrors;
    private ArrayList<String> letters = new ArrayList<>();

    private String nextWordToFind(){
        return WORDS[RANDOM.nextInt(WORDS.length)];
    }

    public void newGame(){
        System.out.println("Alle Buchstaben muessen Groß geschrieben werden");
        nbErrors = 0;
        letters.clear();
        wordToFind = nextWordToFind();

        wordFound = new char[wordToFind.length()];

        for(int i = 0; i<wordFound.length;i++){
            wordFound[i]='_';
        }
    }

    public boolean wordFound(){
        return wordToFind.contentEquals(new String(wordFound));
    }

    private void enter(String c) {
        if(!letters.contains(c)){
             if(wordToFind.contains(c)){
                int index = wordToFind.indexOf(c);

                while (index >= 0){
                    wordFound[index] = c.charAt(0);
                    index = wordToFind.indexOf(c, index + 1);
                }
             }else{
                nbErrors++;
             }

             letters.add(c);
        }
    }

    private String wordFoundContent(){
        StringBuilder builder = new StringBuilder();

        for(int i=0; i<wordFound.length; i++){
            builder.append(wordFound[i]);

            if(i<wordFound.length - 1){
                builder.append(" ");
            }
        }
        return builder.toString();
    }

    public void play(){
        try(Scanner input = new Scanner(System.in)){
            while(nbErrors<MAX_ERRORS){
                System.out.println("\nGib einen Buchstaben ein:");
                //get next input from user
                String str = input.next();

                if(str.length() >1){
                    str = str.substring(0,1);
                }

                //update word found
                enter(str);

                System.out.println("\n" + wordFoundContent());

                //check if word is found
                if(wordFound()){
                    System.out.println("\nYou win!");
                    break;
                }else{

                    System.out.println("\n=> Nb tries remaining: "+ (MAX_ERRORS - nbErrors));
                }
            }

            if (nbErrors == MAX_ERRORS){

                System.out.println("\nYou lose!");
                System.out.println("=> Word to find was: " + wordToFind);
            }
        }
    }


    public static void main(String[] args){
        Hangman hangmanGame = new Hangman();
        hangmanGame.newGame();
        hangmanGame.play();
    }

}
